<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\APIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/boards-of-directors', [APIController::class, 'boardOfDirectors'])->name('api.board_of_directors');
Route::get('/supervisors/boards-of-directors/{id}', [APIController::class, 'supervisorsByBoardOfDirector'])->name('api.supervisors.board_of_director');
Route::get('/managers/supervisors/{id}', [APIController::class, 'managersBySupervisor'])->name('api.managers.supervisor');