<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class APIController extends Controller {
    private $api_host = "http://localhost:3000/";

    protected $client;

    public function __construct() {
        $this->client = new Client(['http_errors' => false]);
    }

    private function sendRequest($url) {
        $response = $this->client->request('GET', $url, [
            'verify'  => false,
        ]);

        if($response->getStatusCode() !== 200) {
            return [ "success" => false, "content" => null, "error" => ["code" => $response->getStatusCode()] ];
        }

        $responseBody = json_decode($response->getBody());

        return [ "success" => true, "content" => $responseBody ];
    }

    public function boardOfDirectors() {
        $url = $this->api_host . "board-of-directors";

        return response()->json($this->sendRequest($url));
    }

    public function supervisorsByBoardOfDirector($id) {
        $url = $this->api_host . "supervisors/board-of-director/" . $id;

        return response()->json($this->sendRequest($url));
    }

    public function managersBySupervisor($id) {
        $url = $this->api_host . "managers/supervisor/" . $id;

        return response()->json($this->sendRequest($url));
    }
    
}
