# Digi Test

Projeto feito com duas aplicações. 
Uma em Laravel/Vue que fica encarregada de receber e mostrar os dados ao usuário e a outra com Nestjs que tem os dados requisitados.

## Getting Started

### Prerequisites

Ter PHP e Composer instalado para o projeto de Laravel e ter o NPM instalado para o Nestjs.

### Installing

## Laravel

**1:** Abra o cmd no caminho da pasta 'laravel'.

**2:** Execute o comando 'composer install'.

**3:** Crie um arquivo '.env' na raiz do projeto seguindo as variáveis do arquivo '.env.example'.

**4:** Execute o comando 'php artisan key:generate'.

**5:** Execute o comando 'php artisan serve' para rodar o servidor.

Pronto, sua aplicação já está rodando no endereço 'localhost:8000'.

## Nestjs

**1:** Abra o cmd no caminho da pasta 'nestjs'.

**2:** Execute o comando 'npm install'.

**3:** Execute o comando 'npm run start'.

Pronto, a aplicação estará rodando no endereço 'localhost:3000'.

Com as duas aplicações rodando elas já podem ser usadas com os dados sendo consumidos do Nestjs e exibidos no Laravel.

## Authors

* **Lucas Toledo Heilig** - [Toledolu](https://gitlab.com/Toledolu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
