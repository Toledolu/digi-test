import { HttpException, Injectable } from '@nestjs/common';
import { Supervisors } from './supervisors.mock';
import { Supervisor } from './supervisors.interface';

@Injectable()
export class SupervisorsService {
  supervisors = Supervisors;

  getSupervisors(): Promise<Supervisor[]> {
    return new Promise((resolve) => {
      resolve(this.supervisors);
    });
  }

  getSupervisor(id: Number): Promise<Supervisor> {
    return new Promise((resolve) => { 
      const supervisor = this.supervisors.find((supervisor) => { return supervisor.id == id });

      if(!supervisor) throw new HttpException('No supervisor found with the ID provided.', 404);

      resolve(supervisor);
    });
  }

  getSupervisorsByBoardOfDirector(id: Number): Promise<Supervisor[]> {
    return new Promise((resolve) => {
      const supervisors = this.supervisors.filter((supervisor) => { return supervisor.boardId == id });

      if(supervisors.length == 0) throw new HttpException('No supervisors found with this boardId provided.', 404);

      resolve(supervisors);
    });
  }
}
