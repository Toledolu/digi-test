export interface Supervisor {
  boardId: number;
  id: number;
  name: string;
}