import { Controller, Get, Param } from '@nestjs/common';
import { SupervisorsService } from './supervisors.service';
import { Supervisor } from './supervisors.interface';

@Controller('supervisors')
export class SupervisorsController {
  constructor(private supervisorsService: SupervisorsService) {}

  @Get()
  async getSupervisors(): Promise<Supervisor[]> {
    return this.supervisorsService.getSupervisors();
  }

  @Get(':id')
  async getSupervisor(@Param() params): Promise<Supervisor> {
    return this.supervisorsService.getSupervisor(params.id);
  }

  @Get('/board-of-director/:id')
  async getSupervisorsByBoardOfDirector(@Param() params): Promise<Supervisor[]> {
    return this.supervisorsService.getSupervisorsByBoardOfDirector(params.id);
  }
}
