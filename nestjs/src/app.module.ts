import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ManagersModule } from './managers/managers.module';
import { SupervisorsModule } from './supervisors/supervisors.module';
import { BoardOfDirectorsModule } from './board-of-directors/board-of-directors.module';

@Module({
  imports: [ManagersModule, SupervisorsModule, BoardOfDirectorsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
