export interface Manager {
  supervisorId: number;
  id: number;
  name: string;
}