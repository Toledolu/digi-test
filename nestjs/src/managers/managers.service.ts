import { HttpException, Injectable } from '@nestjs/common';
import { Managers } from './managers.mock';
import { Manager } from './managers.interface';

@Injectable()
export class ManagersService {
  managers = Managers;

  getManagers(): Promise<Manager[]> {
    return new Promise((resolve) => {
      resolve(this.managers);
    });
  }

  getManager(id: Number): Promise<Manager> {
    return new Promise((resolve) => { 
      const manager = this.managers.find((manager) => { return manager.id == id });

      if(!manager) throw new HttpException('No manager found with the ID provided.', 404);

      resolve(manager);
    });
  }

  getManagersBySupervisor(id: Number): Promise<Manager[]> {
    return new Promise((resolve) => {
      const managers = this.managers.filter((manager) => { return manager.supervisorId == id });

      if(managers.length == 0) throw new HttpException('No managers found with this supervisorId provided.', 404);

      resolve(managers);
    });
  }
}
