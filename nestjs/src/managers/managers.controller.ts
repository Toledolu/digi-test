import { Controller, Get, Param } from '@nestjs/common';
import { ManagersService } from './managers.service';
import { Manager } from './managers.interface';

@Controller('managers')
export class ManagersController {
  constructor(private managersService: ManagersService) {}

  @Get()
  async getManagers(): Promise<Manager[]> {
    return this.managersService.getManagers();
  }

  @Get(':id')
  async getManager(@Param() params): Promise<Manager> {
    return this.managersService.getManager(params.id);
  }

  @Get('/supervisor/:id')
  async getManagersBySupervisor(@Param() params): Promise<Manager[]> {
    return this.managersService.getManagersBySupervisor(params.id);
  }
}
