import { Test, TestingModule } from '@nestjs/testing';
import { BoardOfDirectorsService } from './board-of-directors.service';

describe('BoardOfDirectorsService', () => {
  let service: BoardOfDirectorsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BoardOfDirectorsService],
    }).compile();

    service = module.get<BoardOfDirectorsService>(BoardOfDirectorsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
