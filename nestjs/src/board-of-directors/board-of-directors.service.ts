import { HttpException, Injectable } from '@nestjs/common';
import { BoardOfDirectors } from './board-of-directors.mock';
import { BoardOfDirector } from './board-of-directors.interface';

@Injectable()
export class BoardOfDirectorsService {
  boardOfDirectors = BoardOfDirectors;

  getBoardOfDirectors(): Promise<BoardOfDirector[]> {
    return new Promise((resolve) => {
      resolve(this.boardOfDirectors);
    });
  }

  getBoardOfDirector(id: Number): Promise<BoardOfDirector> {
    return new Promise((resolve) => { 
      const boardOfDirector = this.boardOfDirectors.find((boardOfDirector) => { return boardOfDirector.id == id });

      if(!boardOfDirector) throw new HttpException('No boardOfDirector found with the ID provided.', 404);

      resolve(boardOfDirector);
    });
  }
}
