import { Controller, Get, Param } from '@nestjs/common';
import { BoardOfDirectorsService } from './board-of-directors.service';
import { BoardOfDirector } from './board-of-directors.interface';

@Controller('board-of-directors')
export class BoardOfDirectorsController {
  constructor(private boardOfDirectorsService: BoardOfDirectorsService) {}

  @Get()
  async getBoardOfDirectors(): Promise<BoardOfDirector[]> {
    return this.boardOfDirectorsService.getBoardOfDirectors();
  }

  @Get(':id')
  async getBoardOfDirector(@Param() params): Promise<BoardOfDirector> {
    return this.boardOfDirectorsService.getBoardOfDirector(params.id);
  }
}
