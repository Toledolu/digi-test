import { Test, TestingModule } from '@nestjs/testing';
import { BoardOfDirectorsController } from './board-of-directors.controller';

describe('BoardOfDirectorsController', () => {
  let controller: BoardOfDirectorsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BoardOfDirectorsController],
    }).compile();

    controller = module.get<BoardOfDirectorsController>(BoardOfDirectorsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
