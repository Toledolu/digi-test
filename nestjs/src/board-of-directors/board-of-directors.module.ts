import { Module } from '@nestjs/common';
import { BoardOfDirectorsController } from './board-of-directors.controller';
import { BoardOfDirectorsService } from './board-of-directors.service';

@Module({
  controllers: [BoardOfDirectorsController],
  providers: [BoardOfDirectorsService]
})
export class BoardOfDirectorsModule {}
