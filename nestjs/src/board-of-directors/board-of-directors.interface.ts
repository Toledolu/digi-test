export interface BoardOfDirector {
  id: number;
  name: string;
}